
<?php
$pdo = new PDO("mysql:host=db:3307;dbname=simple_db", 'root', 'root');
$pdo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, false);

$unbufferedResult = $pdo->query("SELECT * FROM Users");
foreach ($unbufferedResult as $row) {
    echo $row['name'], " ", $row['age'], " ", $row['mail'] . PHP_EOL;
}
?>
